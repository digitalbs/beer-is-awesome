//load in apis
var beers = require('./beers'),
    breweries = require('./breweries'),
    search = require('./search');

exports.routes = function () {
    //create new route array for to build/concat full API routes
    var routeArry = [];

    routeArry = routeArry.concat(beers, breweries, search);
    return routeArry;
};



