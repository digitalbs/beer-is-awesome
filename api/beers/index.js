/**
 *
 * Load in modules
 */
var request = require('request'),
    configBuilder = require('../../config/proxy');

/**
 *
 * @type {Array}
 * Export Routes for beers
 */
module.exports = [
    {
        method: 'GET',
        path: '/beers/{id?}',
        config:{
            handler: getBeers
        }
    }
];

function getBeers(req){
    //build request config for beers
    var url;
    if(req.params.id) {
        url = 'beer/' + req.params.id;
    } else {
        url = 'beers/';
    }

    var config = configBuilder.buildConfig(url, req.url.search);

    //after pulling the data back, callback handles what we do with that data
    request.get(config, function(err, res, body) {
        if(typeof body !== "object") {
            body = JSON.parse(body);
        }

        //error check
        if(err) {
            console.error(err);
        }


        if(!err && res.statusCode === 200) {
            req.reply(body);
        }
    });
}



