/**
 *
 * Load in modules
 */
var request = require('request'),
    configBuilder = require('../../config/proxy');

/**
 *
 * @type {Array}
 * Export Routes for breweries
 */
module.exports = [
    {
        method: 'GET',
        path: '/brewery/{id?}',
        config:{
            handler: getBreweryBeers
        }
    }
];

function getBreweryBeers(req){
    //build request config for beers
    var url;
    if(req.params.id) {
        url = 'brewery/' + req.params.id + '/beers';
    } else {
        req.reply('bad request');
    }

    var config = configBuilder.buildConfig(url, req.url.search);

    //after pulling the data back, callback handles what we do with that data
    request.get(config, function(err, res, body) {
        if(typeof body !== "object") {
            body = JSON.parse(body);
        }

        //error check
        if(err) {
            console.error(err);
        }


        if(!err && res.statusCode === 200) {
            req.reply(body);
        }
    });
}



