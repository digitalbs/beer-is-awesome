/**
 *
 * Load in modules
 */
var request = require('request'),
    configBuilder = require('../../config/proxy');

/**
 *
 * @type {Array}
 * Export Routes for search
 */
module.exports = [
    {
        method: 'GET',
        path: '/search',
        config:{
            handler: search
        }
    },
    {
        method: 'GET',
        path: '/search/geosearch',
        config:{
            handler: geoSearch
        }
    }
];

function search(req){
    //build request config for beer search
    var url = 'search';
    var config = configBuilder.buildConfig(url, req.url.search);
    processRequest(config, req);
}

function geoSearch(req){
    //build request config for beer search
    var url = 'search/geo/point';
    var config = configBuilder.buildConfig(url, req.url.search);
    processRequest(config, req);
}

function processRequest (config, req) {
    //after pulling the data back, callback handles what we do with that data
    request.get(config, function(err, res, body) {
        if(typeof body !== "object") {
            body = JSON.parse(body);
        }
        //error check
        if(err) {
            console.error(err);
        }

        console.log(body);


        if(!err && res.statusCode === 200) {
            req.reply(body);
        }
    });
}



