/**
 * Load modules
 */
var Hapi = require('hapi');
var config = require('./config');
var plugins = require('./config/plugins');


/**
 * Setup the HTML Server
 */

if (config.debug === true) {
	console.log("Running in debug mode");
	var server = Hapi.createServer(config.hostname, config.port, { cors: true });
} else {
	var server = Hapi.createServer(config.hostname, config.port, {
		//tls: config.tls,
		cors: true
	});
}


server.pack.require(plugins, function (err) {
	if (err) {
		console.log('Failed to load plugins');
	}
});

/**
 * Routes for API
 */
if (config.debug === true) {
	var api = require('./mock').routes();
	server.addRoutes(api);
	
} else {
	var api = require('./api').routes();
	server.addRoutes(api);
}


/**
 * Start server
 */
server.start(function () {
	console.log('Running on port: ' + config.port);
});