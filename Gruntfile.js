module.exports = function (grunt) {
    "use strict";

    var plugins = [
        'grunt-contrib-connect',
        'grunt-contrib-concat',
        'grunt-shell',
        'grunt-concurrent'
    ];


    grunt.initConfig({
        serverRun: 'forever -w',
        shell: {
            runServer: {
                command: '<%= serverRun %> server.js'
            },
            jasmine_tests: {
                command: 'jasmine-node --autotest --junitreport  spec --watch api helper config',
                options: {
                    stdout: true
                }
            }
        },
        concurrent: {
            dev: {
                tasks: ["shell:runServer", "connect", "shell:jasmine_tests"],
                options: {
                    limit: 3,
                    logConcurrentOutput: true
                }
            },
            server: {
                tasks: ["shell:runServer", "connect"],
                options: {
                    limit: 2,
                    logConcurrentOutput: true
                }
            }
        }
    });
    //load in all grunt plugins added in the array at the top of the config
    for (var i = 0; i < plugins.length; i++) {
        grunt.loadNpmTasks(plugins[i]);
    }


    /**
     *  Default task
     *
     **/
    grunt.registerTask('default', 'connect');

    grunt.registerTask('server', 'concurrent:server');

    grunt.registerTask('develop', 'concurrent:dev');

};