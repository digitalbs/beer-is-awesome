var fs = require('fs');

module.exports = {
    debug: false,
    hostname: 'localhost',
    tls: {
        key: fs.readFileSync('./server.key'),
        cert: fs.readFileSync('./server.crt'),
        rejectUnauthorized: false,
        requestCert: true
    },
    cors: {
        origin:['https://localhost:8000']
    },
    port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3000,
    views: {
        path: 'views',
        engines: {
            html: 'handlebars'
        }
    },
    urls:  {
        failureRedirect: '/login'
    }
};