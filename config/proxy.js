'use strict';

exports.buildConfig = function (url, query, data) {
    var q = '';
    if(query) {
        q = query.replace('?', '&');
    }

    return {
        'url': 'http://api.brewerydb.com/v2/' + url + '?key=3d8fee014d4a94cdd4f607597a0bf3e0' + q,
        'headers': {
            "Content-Type": "application/json",
            "accept": "application/json"
        },
        'json': (data) ? data : false,
        'rejectUnauthorized': false
    };
};


