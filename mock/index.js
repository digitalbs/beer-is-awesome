'use strict';

//load in apis
var marketitems = require('./marketitems');
var marketitemreviews = require('./marketitemreviews');
var marketsettings = require('./marketsettings');
var marketcategories = require('./marketcategories');

exports.routes = function () {
    //create new route array for to build/concat full API routes
    var routeArry = [];

    routeArry = routeArry.concat(marketitems, marketitemreviews, marketsettings, marketcategories);
    return routeArry;
};
