## Beer Is Awesome API
============================

While not necessary, it is helpful to run this node server using ***forever***. ***Forever*** is a tool that helps run a script continuously, which in our case allows us to keep the node server up even with changes to it.

#### Installing Forever

```
npm install -g forever
```

After pulling down the project from github, run npm install.
```
npm install
```

#### Running Node server

```
forever -w server.js
```

#### Running Tests

```
grunt develop
```


